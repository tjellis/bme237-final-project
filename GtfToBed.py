import gzip

# Function to process a GFF/GTF file and write to a BED file
def process_gff_to_bed(gff_file_path, bed_file_path):
    with open(gff_file_path, 'rt') as infile, open(bed_file_path, 'w') as outfile:
        for line in infile:
            if line.startswith('#'):
                continue  # Skip header lines
            parts = line.strip().split('\t')
            if len(parts) < 9:
                continue  # Skip incomplete lines
            chrom = 'chr'
            start = parts[3]
            end = parts[4]
            feature_type = parts[2]
            # if feature_type == 'gene' or feature_type == 'CDS':
            #     continue
            strand = parts[6]
            attributes = parts[8]
            # Parse the attributes to find the ID or any other relevant information
            # This will depend on the format of the attributes in your GFF/GTF files
            id = attributes.split(';')[0] # You'll need to extract this based on your files' format
            outfile.write(f"{chrom}\t{start}\t{end}\t{feature_type}\t{id}\t{strand}\n")

#Sequence		tRNA     	Bounds   	tRNA	Anti	Intron Bounds	Inf	HMM	2'Str	Hit	      	Isotype	Isotype	Type
#Name    	tRNA #	Begin    	End      	Type	Codon	Begin	End	Score	Score	Score	Origin	Pseudo	CM	Score	
# NC_010364.1   



# Example of processing one file
gff_file_path = '../timmys_project_species/therSp_AM4/refseq/GCF_000151205.2_ASM15120v2_genomic_unzipped.gff'  # Replace with your actual file path
bed_file_path = '../timmys_project_species/timmy_output/therSp_AM4_bedfiles/therSp_AM4_chr_nogeneCDS.bed'  # Directory where the BED files will be saved

# gff_file_path = '../timmys_project_species/haloSali1/refseq/GCF_000069025.1_ASM6902v1_genomic.gff'  # Replace with your actual file path
# bed_file_path = '../timmys_project_species/timmy_output/haloSali_chr.bed'  # Directory where the BED files will be saved
process_gff_to_bed(gff_file_path, bed_file_path)
