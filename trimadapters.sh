#!/bin/bash

input="$1"
while IFS= read -r line
do
 echo $line
 NAME="$(basename $line)"
 echo "${NAME%%.*}_trimmed_UMIremoved_retrim.fq.gz"
 cutadapt -j 36 --no-indels -O 15 -m 15 -a GATCGGAAGAGCACACGTCTGAACTCCAGTCAC -o "${NAME%%.*}_trimmed_retrim.fq.gz" $line |& tee -a "${NAME%%.*}_cutadaptLog_retrim.txt"
 umi_tools extract --extract-method=string --log="${NAME%%.*}_retrim.log" --bc-pattern=NNNNNNN --stdin="${NAME%%.*}_trimmed_retrim.fq.gz" --stdout "${NAME%%.*}_trimmed_UMIremoved_retrim.fq.gz"
 cutadapt -j 8 -u -1 -q 10 -o "${NAME%%.*}_trimmed_UMIremoved_TPRT.fq.gz" "${NAME%%.*}_trimmed_UMIremoved_retrim.fq.gz"
done < "$input"

