#!/usr/bin/env python3

''' 
 Corrections for archaea database to account for variable CCA

 Author: Jesse Leavitt
 Date: 10/18/2022
'''

import os,re,sys

class archaeal_tRAX_db:
    ''' class to make custom archaeal tRAX databases that account for variable CCA tails '''
    def __init__(self, db_path,db_name,trna_cm_path):
        self.db_path = db_path
        self.db_name = db_name
        self.db = db_path+db_name
        self.loci_alignment = self.db+'-trnaloci.stk'
        self.mature_fasta = self.db+'-maturetRNAs.fa'
        self.genome_fasta = self.db+'-tRNAgenome.fa'
        self.mature_cmfile = trna_cm_path+'trnamature-arch.cm'  #db_path+'trnamature-arch.cm' # this can be updated with the path to tRAX file which contains cm models
        self.archpositions = list([-1,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,'e1','e2','e3','e4','e5','e6','e7','e8','e9','e10','e11','e12','e13','e14','e15','e16','e17',46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76])
    
    def identify_tRNA_needing_CCA(self):
        ''' genomically encoded CCA tRNAs will have a signature CCA--- in the loci secondary structure alignment '''

        nonCCA_tRNAs_1 = []
        nonCCA_tRNAs_2 = []
        with open(self.loci_alignment,'r') as f:
            for line in f:
                if line.startswith('#=GR'):
                    # if the line ends with ... then the gap is conserved
                    if line.rstrip().endswith('...'):
                        # the tRNA ID losses it's loci numer e.g. tRNA-Lys-TTA-1-2 becomes tRNA-Lys-TTA-1 
                        # so the list appends slices of the name to match with Andrew's list
                        nonCCA_tRNAs_1.append(line.split()[1][:-2])
                if line.startswith('tRNA'):
                    
                    # if line ends with --- then it is not a genomically encoded CCA tRNA
                    if line.rstrip().endswith('---'):
                    
                        nonCCA_tRNAs_2.append(line.split()[0][:-2])

        # find the elements that are in both lists to prevent counting tRNAs that might have a gap at 3' end
        nonCCA_tRNAs = list(set(nonCCA_tRNAs_1) & set(nonCCA_tRNAs_2))

        return nonCCA_tRNAs

    def add_CCA_to_3end(self,tRNA_dict):
        ''' add CCA to the 3' end of the tRNA sequence if it doesn't already have one '''

        # count how many Ns are on the 3' end of the tRNA
        N_count = 0
        for tRNA, seq  in tRNA_dict.items():
            seq = seq[::-1]
            i = 0
            while seq[i] == 'N':
                N_count += 1
                i += 1
            break
        nonCCA_tRNAs = self.identify_tRNA_needing_CCA()

        # add the CCA to the 3' end of the coding sequence if it doesn't already have one
        for tRNA in tRNA_dict:
            if tRNA in nonCCA_tRNAs:
                tRNA_dict[tRNA] = tRNA_dict[tRNA][:-N_count]+'CCA'+tRNA_dict[tRNA][-N_count:]      
        return tRNA_dict

    def rewrite_maturetRNA_fasta(self):
        ''' read the maturetRNA fasta file and return a dictionary of the mature tRNA sequences '''

        # read in mature tRNA fasta file
        maturetRNA_dict = {}
        with open(self.mature_fasta, 'r') as file:
            for line in file:
                if line.startswith('>'):
                    maturetRNA = line.strip().split(' ')[0][1:]
                    maturetRNA_dict[maturetRNA] = ''
                else:
                    maturetRNA_dict[maturetRNA] += line.strip()
        
        # reasign the maturetRNA_dict to have CCA added to the 3' end of the tRNA
        self.mature_fasta = self.add_CCA_to_3end(maturetRNA_dict)

        # write the new maturetRNA fasta file to db_path
        with open(self.db+'-maturetRNAs.fa', 'w') as file:
            for tRNA in self.mature_fasta:
                file.write('>'+tRNA+'\n')
                file.write(self.mature_fasta[tRNA]+'\n')


    def rewrite_tRNAgenome_fasta(self):
        ''' read the tRNAGenome fasta file and return a dictionary of the genome sequences '''

        # read in genome fasta file
        tRNAGenome_dict = {}
        with open(self.genome_fasta, 'r') as file:
            for line in file:
                if line.startswith('>'):
                    genome = line.strip().split(' ')[0][1:]
                    tRNAGenome_dict[genome] = ''
                else:
                    tRNAGenome_dict[genome] += line.strip()
        
        # reasign the tRNAGenome_dict to have CCA added to the 3' end of the tRNA
        for tRNA in self.mature_fasta:
            if tRNA in tRNAGenome_dict.keys():
                tRNAGenome_dict[tRNA] = self.mature_fasta[tRNA]

        # write the new tRNAGenome fasta file to db_path
        with open(self.db+'-tRNAgenome.fa', 'w') as file:
            for genome in tRNAGenome_dict:
                file.write('>'+genome+'\n')
                # if the genome is chr write new line every 70 bases
                if genome.startswith('chr'):
                    for i in range(0,len(tRNAGenome_dict[genome]),70):
                        file.write(tRNAGenome_dict[genome][i:i+70]+'\n')
                else:
                    file.write(tRNAGenome_dict[genome]+'\n')
                
    def realign_maturetRNA(self):
        ''' re-align the maturetRNA.fa file to make a new trnaalign.stk file '''

        # make a temporary fasta file with the mature tRNA sequences that dones't include Ns
        with open(self.db+'-maturetRNAs_temp.fa', 'w') as file:
            for tRNA in self.mature_fasta:
                file.write('>'+tRNA+'\n')
                file.write(self.mature_fasta[tRNA].replace('N','')+'\n')

        # align the temporary fasta file with infernal
        cmcommand = ['cmalign', '-o', self.db+'-trnaalign.stk', '--nonbanded','--notrunc', '-g', self.mature_cmfile, self.db+'-maturetRNAs_temp.fa']
        os.system(' '.join(cmcommand))

        # remove the temporary fasta file
        os.system('rm '+self.db+'-maturetRNAs_temp.fa')

    def get_align_consensus(self):
        ''' get the consensus sequence from the trnaalign.stk file '''

        # read in the trnaalign.stk file
        with open(self.db+'-trnaalign.stk', 'r') as file:
            for line in file:
                if line.startswith('#=GC'):
                    token = line.split(' ')[1]
                    if 'SS_cons' in token:
                        SS_cons = line.split('SS_cons')[1]
                    if 'RF' in token:
                        RF = line.split('RF')[1]
            self.align_consensus = (SS_cons, RF)
            

    def sprinzl_positions(self):
        ''' 
            assign sprinzl positions to the consensus aligment
            this function is based on gettrnanums() from tRAX (Andrew et al. 2022)
        '''

        trnapos = []
        currcount = 0
        enum = 1
        gapnum = 1
        intronnum = 1
        margin = 0
        
        for i in range(margin):
            trnapos.append('head'+str(margin-i))

        self.get_align_consensus()
        for i, struct in enumerate(self.align_consensus[1].strip()):
            if currcount >= len(self.archpositions):
                trnapos.append('gap'+str(gapnum))
                gapnum +=1
                currcount += 1
            elif struct in set("+=*"):
                if currcount == 0 and struct == '=':
                    currcount = 1
                    gapnum = 1
                if self.archpositions[currcount] == 'e':
                    trnapos.append('e'+str(enum))
                    enum += 1
                    currcount += 1
                    gapnum = 1
                elif self.archpositions[currcount] == '-':
                    trnapos.append(str(currcount)+'.gap'+str(gapnum))
                    gapnum += 1
                    currcount += 1
                else:
                    trnapos.append(str(self.archpositions[currcount]))
                    currcount += 1
                    gapnum = 1
            else:
            
                if self.archpositions[currcount] == 38:
                    trnapos.append('intron'+str(intronnum))
                    intronnum += 1
                else:
                    
                    trnapos.append(str(currcount)+'.gap'+str(gapnum))
                    gapnum += 1
        for i in range(margin):
            trnapos.append('tail'+str(i+1))

        return trnapos
        

    def rewrite_alignnum_text(self):
        ''' rewrite the alignnum.txt file to include the sprinzl positions '''

        # call the function to get the sprinzl positions and alignments
        sprinzl_positions = self.sprinzl_positions()
        consensus, structure = self.align_consensus[0].strip(),self.align_consensus[1].strip()

        # write the new alignnum.txt file to db_path
        with open(self.db+'-alignnum.txt', 'w') as file:
            for i, pos in enumerate(sprinzl_positions):
                file.write('\t'.join([str(i), structure[i], consensus[i], pos+'\n']))
              
    def rewrite_maturetRNA_bed(self):
        ''' rewrite the maturetRNA.bed file to include the sprinzl positions '''

        # remove the old maturetRNA.bed file
        os.system('rm '+self.db+'-maturetRNAs.bed')

        # read mature tRNA fasta and make new bedfile
        with open(self.db+'-maturetRNAs.fa', 'r') as file:
            for line in file:
                if line.startswith('>'):
                    tRNA = line.strip().split(' ')[0][1:]
                    if tRNA in self.mature_fasta.keys():
                        with open(self.db+'-maturetRNAs.bed', 'a') as bedfile:

                            # count number of Ns on either side of the mature tRNA
                            leftN = len(self.mature_fasta[tRNA]) - len(self.mature_fasta[tRNA].lstrip('N'))
                            rightN = len(self.mature_fasta[tRNA]) - len(self.mature_fasta[tRNA].rstrip('N'))
                            full_length = len(self.mature_fasta[tRNA])

                            # write the bed file
                            bedfile.write('\t'.join([tRNA, str(leftN), str(full_length-(rightN)), tRNA, '1000','+'+'\n']))

    def reindex_tRNAgenome(self):
        ''' use bowtie2 to index the new tRNA genome '''
    
        # remove all previous files with extension .1.bt2, .2.bt2, .3.bt2, .4.bt2, .rev.1.bt2, .rev.2.bt2
        
        # regular expression to find numbers after the . in the file extension
        regex = re.compile(r'\.\d+\.bt2')
        for file in os.listdir(self.db_path):
            if regex.search(file):
                os.system('rm '+self.db_path+file)

        # use bowtie2 to build new index of the genome
        os.system('bowtie2-build '+self.db+'-tRNAgenome.fa --large-index '+self.db+'-tRNAgenome')


if __name__ == "__main__":


     
    import argparse

    # make the command line arguments
    parser = argparse.ArgumentParser(description='Process db_path')
    parser.add_argument('--db_path', type=str, help='path to the database')
    parser.add_argument('--db_name', type=str, help='name of the database in path')
    parser.add_argument('--trna_cm', type=str, help='path to the tRNA covariance model')
    args = parser.parse_args()


    db_path = args.db_path
    db_name = args.db_name
    trna_cm_path = args.trna_cm
    #db_path = '/home/jovyan/data/tRAX-dbs/methMari_S2_test/'
    #db_name = 'metMar_S2_test'
    archaeal_tRAX_db = archaeal_tRAX_db(db_path,db_name, trna_cm_path)
    archaeal_tRAX_db.rewrite_maturetRNA_fasta()
    archaeal_tRAX_db.rewrite_tRNAgenome_fasta()
    archaeal_tRAX_db.realign_maturetRNA()
    archaeal_tRAX_db.rewrite_alignnum_text()
    archaeal_tRAX_db.rewrite_maturetRNA_bed()
    archaeal_tRAX_db.reindex_tRNAgenome()


